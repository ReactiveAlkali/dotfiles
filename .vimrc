" vim-plug
call plug#begin('~/.vim/plugged')
Plug 'arcticicestudio/nord-vim'
Plug 'vim-airline/vim-airline'
call plug#end()

colorscheme nord

set updatetime=100
set number

packloadall
silent! helptags ALL
